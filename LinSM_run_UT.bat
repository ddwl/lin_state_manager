gcc -fprofile-arcs -ftest-coverage -I src -I src\include -g unit_tests\UT_LinSM.c -o bin\UT_LinSM.exe 
bin\UT_LinSM.exe
gcov UT_LinSM.c
python -m gcovr -r . --html --html-details -o .\doc\coverage.html
pause
