/*------------------------------------------------------------
 * @file UT_LinSM.c
 *
 * @brief Unit tests for LIN State Manager Module (LinSM.c)
 * PSES project - LIN State Manager
 *
 * @see AUTOSAR Classic Platform release R20-11
 * 
 * @authors Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *----------------------------------------------------------*/

#include "Std_Types.h"
 
#include "acutest.h"
#include "fff.h"
#include "LinSM.h"

DEFINE_FFF_GLOBALS;

FAKE_VALUE_FUNC(Std_ReturnType, LinIf_GotoSleep, NetworkHandleType);
FAKE_VALUE_FUNC(Std_ReturnType, LinIf_ScheduleRequest, NetworkHandleType, LinIf_SchHandleType);
FAKE_VALUE_FUNC(Std_ReturnType, LinIf_Wakeup, NetworkHandleType);
FAKE_VOID_FUNC(ComM_BusSM_ModeIndication, NetworkHandleType, ComM_ModeType);
FAKE_VOID_FUNC(ComM_BusSM_BusSleepMode, NetworkHandleType);
FAKE_VOID_FUNC(BswM_LinSM_CurrentSchedule, NetworkHandleType, LinIf_SchHandleType);
FAKE_VOID_FUNC(BswM_LinSM_CurrentState, NetworkHandleType, LinSM_ModeType);

#include "LinSM.c"

void ResetAll(void){
  RESET_FAKE(LinIf_GotoSleep);
  RESET_FAKE(LinIf_ScheduleRequest);
  RESET_FAKE(LinIf_Wakeup);
  RESET_FAKE(ComM_BusSM_ModeIndication);
  RESET_FAKE(ComM_BusSM_BusSleepMode);
  RESET_FAKE(BswM_LinSM_CurrentSchedule);
  RESET_FAKE(BswM_LinSM_CurrentState);
}

void Test_Of_LinSM_Init(void){
	LinSM_ConfigType* ConfigPtr;
  LinSM_Init(ConfigPtr);

  //[SWS_LinSM_00152], [SWS_LinSM_00160]
  TEST_CHECK(LinSMNetworkStatus == LINSM_NO_COM);

  //[SWS_LinSM_00043] 
  TEST_CHECK(ScheduleRequestTimer == 0);
  TEST_CHECK(GoToSleepTimer == 0);
  TEST_CHECK(WakeUpTimer == 0);

  //[SWS_LinSM_00216] 
  TEST_CHECK(LinSM_ScheduleTable == NULL_SCHEDULE);

  //[SWS_LinSM_00025]
  TEST_CHECK(LinSMState == LINSM_INIT); 
}

void Test_Of_LinSM_MainFunction(void){
  //[SWS_LinSM_00101]
  void ResetAll();
  LinSM_ScheduleTable     = 10;
  ScheduleRequestTimer    = 20;
  GoToSleepTimer          = 30;
  WakeUpTimer             = 40;
  LinSM_MainFunction();
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
  TEST_CHECK(ScheduleRequestTimer == 19);
  TEST_CHECK(GoToSleepTimer == 29);
  TEST_CHECK(WakeUpTimer == 39);

  //[SWS_LinSM_00214]
  void ResetAll();
  LinSM_ScheduleTable     = 10;
  ScheduleRequestTimer    = 1;
  GoToSleepTimer          = 30;
  WakeUpTimer             = 40;
  LinSM_MainFunction();
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == 1);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == LinSM_ScheduleTable);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
  TEST_CHECK(ScheduleRequestTimer == 0);
  TEST_CHECK(GoToSleepTimer == 29);
  TEST_CHECK(WakeUpTimer == 39);

  //[SWS_LinSM_00101]
  void ResetAll();
  LinSM_ScheduleTable     = 10;
  ScheduleRequestTimer    = 20;
  GoToSleepTimer          = 1;
  WakeUpTimer             = 40;
  LinSM_MainFunction();
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
  TEST_CHECK(ScheduleRequestTimer == 19);
  TEST_CHECK(GoToSleepTimer == 0);
  TEST_CHECK(WakeUpTimer == 39);

  //[SWS_LinSM_00101] [SWS_LinSM_00170] [SWS_LinSM_00215]
  void ResetAll();
  LinSM_ScheduleTable     = 10;
  ScheduleRequestTimer    = 20;
  GoToSleepTimer          = 30;
  WakeUpTimer             = 1;
  LinSM_MainFunction();
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_NO_COMMUNICATION);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == 1);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_NO_COM);
  TEST_CHECK(ScheduleRequestTimer == 19);
  TEST_CHECK(GoToSleepTimer == 29);
  TEST_CHECK(WakeUpTimer == 0);
}


void Test_Of_LinSM_ScheduleRequest(void)
{
  Std_ReturnType RetVal;
  NetworkHandleType network;
  LinIf_SchHandleType schedule;

  //[SWS_LinSM_00116]
  void ResetAll();
  network             = 5;
  schedule            = 20;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_UNINIT;
  LinSM_ScheduleTable = 0;

  RetVal = LinSM_ScheduleRequest(network, schedule);
  TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00114]
  void ResetAll();
  network             = 0;
  schedule            = 20;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_INIT;
  LinSM_ScheduleTable = 0;

  RetVal = LinSM_ScheduleRequest(network, schedule);
  TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00115]
  void ResetAll();
  network             = 5;
  schedule            = NULL_SCHEDULE;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_INIT;
  LinSM_ScheduleTable = 0;

  RetVal = LinSM_ScheduleRequest(network, schedule);
  TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_10211]
  void ResetAll();
  network             = 5;
  schedule            = 20;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_INIT;
  LinSM_ScheduleTable = 0;

  RetVal = LinSM_ScheduleRequest(network, schedule);
  TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 0);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00213]
  void ResetAll();
  network             = 5;
  schedule            = 20;
  LinSMNetworkStatus  = LINSM_FULL_COM;
  LinSMState          = LINSM_INIT;
  LinSM_ScheduleTable = 0;
  LinIf_ScheduleRequest_fake.return_val = E_NOT_OK;

  RetVal = LinSM_ScheduleRequest(network, schedule);
  TEST_CHECK(LinIf_ScheduleRequest_fake.call_count == 1);
  TEST_CHECK(LinIf_ScheduleRequest_fake.arg0_val == network);
  TEST_CHECK(LinIf_ScheduleRequest_fake.arg1_val == schedule);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == LinSM_ScheduleTable);
  TEST_CHECK(RetVal == E_NOT_OK);
}

void Test_Of_LinSM_GetVersionInfo(void)
{
  // [SWS_LinSM_00119]
  void ResetAll();
  LinSM_GetVersionInfo(NULL);

}



void Test_Of_LinSM_GetCurrentComMode(void)
{
  NetworkHandleType network;
  ComM_ModeType mode;
  Std_ReturnType RetVal;

  //[SWS_LinSM_00182]
  void ResetAll();
  network             = 5;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_UNINIT;
  RetVal = LinSM_GetCurrentComMode(network, &mode);
  TEST_CHECK(mode == COMM_NO_COMMUNICATION);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00123]
  void ResetAll();
  network             = 0;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_INIT;
  RetVal = LinSM_GetCurrentComMode(network, &mode);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00124]
  network             = 5;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_INIT;
  RetVal = LinSM_GetCurrentComMode(network, NULL);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00181]
  void ResetAll();
  network             = 5;
  LinSMState          = LINSM_INIT;
  LinSMNetworkStatus  = LINSM_RUN_COMMUNICATION;

  RetVal = LinSM_GetCurrentComMode(network, &mode);
  TEST_CHECK(mode == COMM_FULL_COMMUNICATION);
  TEST_CHECK(RetVal == E_OK);

  //[SWS_LinSM_00180]
  void ResetAll();
  network             = 5;
  LinSMNetworkStatus  = LINSM_NO_COM;
  LinSMState          = LINSM_INIT;

  RetVal = LinSM_GetCurrentComMode(network, &mode);
  TEST_CHECK(mode == COMM_NO_COMMUNICATION);
  TEST_CHECK(RetVal == E_OK);
}


void Test_Of_LinSM_RequestComMode(void)
{
  NetworkHandleType network;
  ComM_ModeType mode;
  Std_ReturnType RetVal;

  //[SWS_LinSM_00127]
  void ResetAll();
  network                  = 0;
  mode                     = COMM_NO_COMMUNICATION;
  LinSMState               = LINSM_UNINIT;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_RUN_COMMUNICATION;
  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00128]
  void ResetAll();
  network                  = 1;
  mode                     = COMM_NO_COMMUNICATION;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMState               = LINSM_UNINIT;
  LinSMCommunicationStatus = LINSM_RUN_COMMUNICATION;;
  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00128]
  void ResetAll();
  network                  = 5;
  LinSMState               = LINSM_INIT;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_RUN_COMMUNICATION;
  mode                     = 5;
  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00183]
  void ResetAll();
  network                  = 1;
  LinSMState               = LINSM_INIT;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_RUN_COMMUNICATION;
  mode                     = COMM_SILENT_COMMUNICATION;
  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00035] [SWS_LinSM_00100] [SWS_LinSM_10208] [SWS_LinSM_00036] [SWS_LinSM_00302]
  void ResetAll();
  network                 = 1;
  mode                    = COMM_NO_COMMUNICATION;
  LinSMState              = LINSM_INIT;
  LinSMNetworkStatus      = LINSM_FULL_COM;
  LinSMCommunicationStatus= LINSM_RUN_COMMUNICATION;
  LinIf_GotoSleep_fake.return_val = E_OK;
  
  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(LinIf_GotoSleep_fake.arg0_val == network);
  TEST_CHECK(LinIf_GotoSleep_fake.call_count == 1);
  TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
  TEST_CHECK(LinSMCommunicationStatus == LINSM_GOTO_SLEEP);
  TEST_CHECK(RetVal == E_OK);


  //[SWS_LinSM_00177]
  void ResetAll();
  network                 = 1;
  mode                    = COMM_NO_COMMUNICATION;
  LinSMState              = LINSM_INIT;
  LinSMNetworkStatus      = LINSM_FULL_COM;
  LinSMCommunicationStatus= LINSM_RUN_COMMUNICATION;
  LinIf_GotoSleep_fake.return_val = E_NOT_OK;
  
  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(LinIf_GotoSleep_fake.arg0_val == network);
  TEST_CHECK(LinIf_GotoSleep_fake.call_count == 2);
  TEST_CHECK(LinIf_Wakeup_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(LinSMCommunicationStatus == LINSM_RUN_COMMUNICATION);
  TEST_CHECK(RetVal == E_NOT_OK);

  //[SWS_LinSM_00100] [SWS_LinSM_00047] [SWS_LinSM_00178]
  void ResetAll();
  network                 = 1;
  mode                    = COMM_FULL_COMMUNICATION;
  LinSMState              = LINSM_INIT;
  LinSMNetworkStatus      = LINSM_FULL_COM;
  LinSMCommunicationStatus= LINSM_RUN_COMMUNICATION;
  LinIf_Wakeup_fake.return_val = E_OK;

  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(LinIf_GotoSleep_fake.arg0_val == network);
  TEST_CHECK(LinIf_GotoSleep_fake.call_count == 2);
  TEST_CHECK(LinIf_Wakeup_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(RetVal == E_OK);

  //[SWS_LinSM_00176]
  void ResetAll();
  network                 = 1;
  mode                    = COMM_FULL_COMMUNICATION;
  LinSMState              = LINSM_INIT;
  LinSMNetworkStatus      = LINSM_FULL_COM;
  LinSMCommunicationStatus= LINSM_RUN_COMMUNICATION;
  LinIf_Wakeup_fake.return_val = E_NOT_OK;

  RetVal = LinSM_RequestComMode(network, mode);
  TEST_CHECK(LinIf_GotoSleep_fake.arg0_val == network);
  TEST_CHECK(LinIf_GotoSleep_fake.call_count == 2);
  TEST_CHECK(LinIf_Wakeup_fake.call_count == 2);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(WakeUpTimer == 0);
  TEST_CHECK(RetVal == E_NOT_OK);

  void ResetAll();
  network                 = 1;
  mode                    = COMM_FULL_COMMUNICATION_WITH_WAKEUP_REQUEST;
  LinSMState              = LINSM_INIT;
  LinSMNetworkStatus      = LINSM_FULL_COM;
  LinSMCommunicationStatus= LINSM_RUN_COMMUNICATION;
  LinIf_Wakeup_fake.return_val = E_NOT_OK;
  RetVal = LinSM_RequestComMode(network, mode);
}




void Test_Of_LinSM_ScheduleRequestConfirmation(void)
{
  //[SWS_LinSM_00172]
  NetworkHandleType network;
  LinIf_SchHandleType schedule;

  void ResetAll();
  network                = 5;
  LinSMState             = LINSM_INIT;
  ScheduleRequestTimer   = 10;
  schedule               = 20;
  LinSM_ScheduleTable    = schedule;

  LinSM_ScheduleRequestConfirmation(network, schedule);
  TEST_CHECK(LinSM_ScheduleTable == schedule);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == schedule);
  TEST_CHECK(ScheduleRequestTimer == 0);

  void ResetAll();
  network                = 5;
  LinSMState             = LINSM_INIT;
  ScheduleRequestTimer   = 0;
  schedule               = 20;
  LinSM_ScheduleTable    = schedule;

  LinSM_ScheduleRequestConfirmation(network, schedule);
  TEST_CHECK(LinSM_ScheduleTable == schedule);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == schedule);
  TEST_CHECK(ScheduleRequestTimer == 0);

  //[SWS_LinSM_00206]
  void ResetAll();
  network               = 5;
  schedule              = 20;
  LinSMNetworkStatus    = LINSM_FULL_COM;
  LinSMState            = LINSM_INIT;
  ScheduleRequestTimer  = 10;

  LinSM_ScheduleRequestConfirmation(network, schedule);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.call_count == 2);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentSchedule_fake.arg1_val == schedule);
  TEST_CHECK(ScheduleRequestTimer == 0);
}


void Test_Of_LinSM_GotoSleepConfirmation(void)
{
  //[SWS_LinSM_00136]
  NetworkHandleType network;
  void ResetAll();
  network                  = 10;
  LinSMState               = LINSM_INIT;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMNetworkStatus       = LINSM_FULL_COM;
  GoToSleepTimer           = 50;

  LinSM_GotoSleepConfirmation(network,TRUE);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(GoToSleepTimer == 0);

  //[SWS_LinSM_00046]
  void ResetAll();
  network                  = 10;
  LinSMState               = LINSM_INIT;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMNetworkStatus       = LINSM_FULL_COM;
  GoToSleepTimer           = 0;

  LinSM_GotoSleepConfirmation(network,TRUE);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(GoToSleepTimer == 0);

  //[SWS_LinSM_00137]
  void ResetAll();
  network                  = 10;
  LinSMState               = LINSM_UNINIT;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMNetworkStatus       = LINSM_FULL_COM;
  GoToSleepTimer           = 50;

  LinSM_GotoSleepConfirmation(network,TRUE);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 2);
  TEST_CHECK(GoToSleepTimer == 0);
}



void Test_Of_LinSM_WakeupConfirmation(void){
  NetworkHandleType network;

  //[SWS_LinSM_00154]
  void ResetAll();
  network                  = 1;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMState               = LINSM_INIT;
  WakeUpTimer              = 0;

  LinSM_WakeupConfirmation(network,TRUE);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 0);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 0);
  TEST_CHECK(LinSMNetworkStatus == LINSM_NO_COM);
  TEST_CHECK(LinSMCommunicationStatus == LINSM_GOTO_SLEEP);
  TEST_CHECK(WakeUpTimer == 0);

  //[SWS_LinSM_00049] [SWS_LinSM_00192] [SWS_LinSM_00301] [SWS_LinSM_00033]
  void ResetAll();
  network                  = 1;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMState               = LINSM_INIT;
  WakeUpTimer              = 5;

  LinSM_WakeupConfirmation(network,FALSE);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 1);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_NO_COMMUNICATION);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 1);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_NO_COM);
  TEST_CHECK(LinSMCommunicationStatus == LINSM_GOTO_SLEEP);
  TEST_CHECK(LinSMNetworkStatus == LINSM_NO_COM);
  TEST_CHECK(WakeUpTimer == 0);

  //[SWS_LinSM_00202]
  void ResetAll();
  network                  = 1;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMState               = LINSM_INIT;
  WakeUpTimer              = 5;

  LinSM_WakeupConfirmation(network,FALSE);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 2);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_NO_COMMUNICATION);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 2);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_NO_COM);
  TEST_CHECK(LinSMCommunicationStatus == LINSM_GOTO_SLEEP);
  TEST_CHECK(LinSMNetworkStatus == LINSM_NO_COM);
  TEST_CHECK(WakeUpTimer == 0);

void ResetAll();
  network                  = 1;
  LinSMNetworkStatus       = LINSM_NO_COM;
  LinSMCommunicationStatus = LINSM_GOTO_SLEEP;
  LinSMState               = LINSM_INIT;
  WakeUpTimer              = 5;

  LinSM_WakeupConfirmation(network,TRUE);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.call_count == 3);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg0_val == network);
  TEST_CHECK(ComM_BusSM_ModeIndication_fake.arg1_val == COMM_FULL_COMMUNICATION);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.call_count == 3);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg0_val == network);
  TEST_CHECK(BswM_LinSM_CurrentState_fake.arg1_val == LINSM_FULL_COM);
  TEST_CHECK(LinSMNetworkStatus == LINSM_FULL_COM);
  TEST_CHECK(WakeUpTimer == 0);

}

/*
  List of tests.
*/
TEST_LIST = {
    { "Test of LinSM_Init",                         Test_Of_LinSM_Init                          },
    { "Test of LinSM_MainFunction",                 Test_Of_LinSM_MainFunction                  },
    { "Test of LinSM_ScheduleRequest",              Test_Of_LinSM_ScheduleRequest               },
    { "Test of LinSM_GetVersionInfo",               Test_Of_LinSM_GetVersionInfo                },
    { "Test of LinSM_GetCurrentComMode",            Test_Of_LinSM_GetCurrentComMode             },
    { "Test of LinSM_RequestComMode",               Test_Of_LinSM_RequestComMode                },
    { "Test of LinSM_ScheduleRequestConfirmation",  Test_Of_LinSM_ScheduleRequestConfirmation   },
    { "Test of LinSM_GotoSleepConfirmation",        Test_Of_LinSM_GotoSleepConfirmation         },
    { "Test of LinSM_WakeupConfirmation",           Test_Of_LinSM_WakeupConfirmation            },
    { NULL, NULL }
};

/*------------------------------------------------------------
  -----------------------End-of-file--------------------------
  ------------------------------------------------------------*/
 
