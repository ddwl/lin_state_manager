#ifndef _LIN_GENERALTYPES_H
#define _LIN_GENERALTYPES_H

/*------------------------------------------------------------
 * PSES project - LIN State Manager
 * AUTOSAR Classic Platform release R20-11
 * Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *
 * Lin_GeneralTypes header file
 * NOT COMPLETE, ONLY FOR LinSM IMPLEMENTATION
 *----------------------------------------------------------*/

typedef uint8 LinTrcv_TrcvModeType;

//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_LIN_GENERALTYPES_H
