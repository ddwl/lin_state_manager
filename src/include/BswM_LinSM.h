#ifndef _BSWM_LINSM_H
#define _BSWM_LINSM_H

/*------------------------------------------------------------
 * PSES project - LIN State Manager
 * AUTOSAR Classic Platform release R20-11
 * Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *
 * BswM_LinSM header file
 * NOT COMPLETE, ONLY FOR LinSM IMPLEMENTATION
 *----------------------------------------------------------*/

#include "ComStack_Types.h"
#include "LinSM.h"
#include "LinIf.h"

void BswM_LinSM_CurrentSchedule(NetworkHandleType network, LinIf_SchHandleType CurrentSchedule);

void BswM_LinSM_CurrentState(NetworkHandleType network, LinSM_ModeType CurrentState);

//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_BSWM_LINSM_H
