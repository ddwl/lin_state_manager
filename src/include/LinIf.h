#ifndef _LINIF_H
#define _LINIF_H

/*------------------------------------------------------------
 * PSES project - LIN State Manager
 * AUTOSAR Classic Platform release R20-11
 * Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *
 * LinIf header file
 * NOT COMPLETE, ONLY FOR LinSM IMPLEMENTATION
 *----------------------------------------------------------*/

#include "Std_Types.h"
#include "ComStack_Types.h"

#define NULL_SCHEDULE (0x00)

typedef uint8 LinIf_SchHandleType;

Std_ReturnType LinIf_ScheduleRequest(NetworkHandleType network,LinIf_SchHandleType schedule);

Std_ReturnType LinIf_GotoSleep(NetworkHandleType network);

Std_ReturnType LinIf_Wakeup(NetworkHandleType network);


//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_LINIF_H
