#ifndef _COMM_BUSSM_H
#define _COMM_BUSSM_H

/*------------------------------------------------------------
 * PSES project - LIN State Manager
 * AUTOSAR Classic Platform release R20-11
 * Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *
 * Rte_ComM_Type header file
 * NOT COMPLETE, ONLY FOR LinSM IMPLEMENTATION
 *----------------------------------------------------------*/

#include "Rte_ComM_Type.h"
#include "ComStack_Types.h"

void ComM_BusSM_ModeIndication(NetworkHandleType network, ComM_ModeType ComMode);

void ComM_BusSM_BusSleepMode(NetworkHandleType network);

//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_COMM_BUSSM_H
