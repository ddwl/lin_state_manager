#ifndef _COMSTACK_TYPES_H
#define _COMSTACK_TYPES_H

/*------------------------------------------------------------
 * PSES project - LIN State Manager
 * AUTOSAR Classic Platform release R20-11
 * Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *
 * ComStack_Types header file
 * NOT COMPLETE, ONLY FOR LinSM IMPLEMENTATION
 *----------------------------------------------------------*/

#include "Platform_Types.h"

typedef uint8 NetworkHandleType;

//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_COMSTACK_TYPES_H
