#ifndef _LIN_SM
#define _LIN_SM

/**-----------------------------------------------------------
 * @file LinSM.h
 *
 * @brief LIN State Manager Module header file
 * PSES project - LIN State Manager
 *
 * @see AUTOSAR Classic Platform release R20-11
 * 
 * @authors Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *----------------------------------------------------------*/

//[SWS_LinSM_00219]
#include "Std_Types.h"
#include "ComStack_Types.h"
#include "Rte_ComM_Type.h"
#include "Lin_GeneralTypes.h"
#include "LinIf.h"

#define LINSM_INIT_SERVICE_ID (0x01)
#define LINSM_SCHEDULE_REQUEST_SERVICE_ID (0x10)
#define LINSM_GET_VERSION_INFO_SERVICE_ID (0x02)
#define LINSM_GET_CURRENT_COM_MODE_SERVICE_ID (0x11)
#define LINSM_REQUEST_COM_MODE_SERVICE_ID (0x12)
#define LINSM_MAIN_FUNCTION_SERVICE_ID (0x30)
#define LINSM_SCHEDULE_REQUEST_CONF_SERVICE_ID (0x20)
#define LINSM_GOTO_SLEEP_IND_SERVICE_ID (0x03)
#define LINSM_GOTO_SLEEP_CONF_SERVICE_ID (0x22)
#define LINSM_WAKEUP_CONF_SERVICE_ID (0x21)


/** @brief LinSM_ModeType
 * 
 * @details Type used to report the current mode to the BswM. [SWS_LinSM_00220]
 */ 
typedef enum {
    LINSM_FULL_COM  = 0x01,
    LINSM_NO_COM    = 0x02
}LinSM_ModeType;


/** @brief LinSM_StatusType
 * 
 * @details Type used to store current state of the state machine.[SWS_LinSM_00020]
 */ 
typedef enum {
	LINSM_RUN_COMMUNICATION,
	LINSM_GOTO_SLEEP
}LinSM_CommunicationType;


/** @brief LinSM_StateType
 * 
 * @details Type used to store current state of the state machine.[SWS_LinSM_00020], [SWS_LinSM_00022], [SWS_LinSM_00024]
 */ 
typedef enum {
	LINSM_UNINIT,
	LINSM_INIT
}LinSM_StateType;

//[SWS_LinSM_00221]
/** @brief LinSM_ConfigType
 * 
 * @details Data structure type for the post-build configuration parameters. [SWS_LinSM_00221]
 */ 
typedef struct{ } LinSM_ConfigType;

#include "BswM_LinSM.h" // [SWS_LinSM_00201]
#include "ComM.h"       // [SWS_LinSM_00013]
#include "ComM_BusSM.h" // [SWS_LinSM_00305]

void LinSM_Init(const LinSM_ConfigType* ConfigPtr);

Std_ReturnType LinSM_ScheduleRequest(NetworkHandleType network, LinIf_SchHandleType schedule);

void LinSM_GetVersionInfo(Std_VersionInfoType* versioninfo);

Std_ReturnType LinSM_GetCurrentComMode(NetworkHandleType network, ComM_ModeType* mode);

Std_ReturnType LinSM_RequestComMode(NetworkHandleType network, ComM_ModeType mode);

void LinSM_ScheduleRequestConfirmation(NetworkHandleType network, LinIf_SchHandleType schedule);

// [SWS_LinSM_00243] Function is not available due to master configuration
//void LinSM_GotoSleepIndication(NetworkHandleType Channel);

void LinSM_GotoSleepConfirmation(NetworkHandleType network, boolean success);

void LinSM_WakeupConfirmation(NetworkHandleType network, boolean success);

//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_LIN_SM
