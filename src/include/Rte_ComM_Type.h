#ifndef _RTE_COMM_TYPE_H
#define _RTE_COMM_TYPE_H

/*------------------------------------------------------------
 * PSES project - LIN State Manager
 * AUTOSAR Classic Platform release R20-11
 * Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *
 * Rte_ComM_Type header file
 * NOT COMPLETE, ONLY FOR LinSM IMPLEMENTATION
 *----------------------------------------------------------*/

typedef enum{
   COMM_NO_COMMUNICATION = 0,
   COMM_FULL_COMMUNICATION = 1,
   COMM_SILENT_COMMUNICATION = 2,
   COMM_FULL_COMMUNICATION_WITH_WAKEUP_REQUEST = 3
}ComM_ModeType;

//------------------------------------------------------------
//----------------------End-of-file---------------------------
//------------------------------------------------------------
#endif //_RTE_COMM_TYPE_H
