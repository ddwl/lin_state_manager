/**-----------------------------------------------------------
 * @file LinSM.c
 *
 * @brief LIN State Manager Module
 * PSES project - LIN State Manager
 *
 * @see AUTOSAR Classic Platform release R20-11
 * 
 * @authors Szymon Janik, Michal Wegrzyn, Dawid Wlodarczyk
 * MTM IIst 2020/2021 AGH UST
 *----------------------------------------------------------*/

#include "LinSM.h"

/* For simplification this implementation works with only one network and some requirements were modifed:
 * [SWS_LinSM_00019] - modified: one network
 * [SWS_LinSM_00173] - modified: one network
 * [SWS_LinSM_00021] - not implemented: one network
 * [SWS_LinSM_00152] - modified: one network
 * [SWS_LinSM_00160] - modified: one network
 * [SWS_LinSM_00175] - not implemented: one network
 * [SWS_LinSM_00164] - not implemented
 * [SWS_LinSM_00032] - modified: one network
 */

/* For simplification this implementation is only for master node. Skiped requirements:
 * [SWS_LinSM_00230]
 * [SWS_LinSM_00231]
 * [SWS_LinSM_00232]
 * [SWS_LinSM_00233]
 * [SWS_LinSM_00234]
 * [SWS_LinSM_00235]
 * [SWS_LinSM_00236]
 * [SWS_LinSM_00237]
 */

/* For simplification LinSMTransceiver is not implemented. Skipped requirements:
 * [SWS_LinSM_00203]
 * [SWS_LinSM_00204]
 * [SWS_LinSM_00205]
 */

/* For simplification no error handling is implemented. Skipped requirements:
 * [SWS_LinSM_00053]
 * [SWS_LinSM_00224]
 * [SWS_LinSM_00102]
 *
 * [SWS_LinSM_00114] - LinSM_ScheduleRequest()
 * [SWS_LinSM_00115]
 * [SWS_LinSM_00116]
 *
 * [SWS_LinSM_00119] - LinSM_GetVersionInfo()
 *
 * [SWS_LinSM_00127] - LinSM_RequestComMode() partially skipped
 * [SWS_LinSM_00191]
 * [SWS_LinSM_00128]
 *
 * [SWS_LinSM_00130] - LinSM_ScheduleRequestConfirmation()
 * [SWS_LinSM_00131]
 *
 * [SWS_LinSM_00239] - LinSM_GotoSleepIndication()
 * [SWS_LinSM_00240]
 *
 * [SWS_LinSM_00136] - LinSM_GotoSleepConfirmation()
 * [SWS_LinSM_00137]
 */
#ifndef NULL
   #define NULL (0)
#endif
#define LINSM_SCHEDULE_REQUEST_TIMEOUT (100)
#define LINSM_WAKEUP_TIMEOUT (100)
#define LINSM_GOTO_SLEEP_TIMEOUT (100)

static uint8 ScheduleRequestTimer;
static uint8 GoToSleepTimer;
static uint8 WakeUpTimer;

static LinSM_StateType LinSMState = LINSM_UNINIT;  //[SWS_LinSM_00161] 
static LinSM_ModeType LinSMNetworkStatus;
static LinSM_CommunicationType LinSMCommunicationStatus;
static LinIf_SchHandleType LinSM_ScheduleTable;

//[SWS_LinSM_00155]
/** @brief LinSM_Init
 * 
 * @details This function initializes the LinSM. [SWS_LinSM_00155]
 * 
 * @param [in] ConfigPtr pointer to the LinSM post-build configuration data.
 */ 
void LinSM_Init(const LinSM_ConfigType* ConfigPtr){

   LinSMNetworkStatus = LINSM_NO_COM; //[SWS_LinSM_00152], [SWS_LinSM_00160]
   //[SWS_LinSM_00166] - no notification

   // [SWS_LinSM_00043] 
   ScheduleRequestTimer = 0; 
   GoToSleepTimer = 0;
   WakeUpTimer = 0;

   LinSM_ScheduleTable = NULL_SCHEDULE; //[SWS_LinSM_00216] 
   
   LinSMState = LINSM_INIT; //[SWS_LinSM_00025] 

   //[SWS_LinSM_00151] - no LinSM API function call
}

//[SWS_LinSM_00156]
/** @brief LinSM_MainFunction
 * 
 * @details Periodic function that runs the timers of different request timeout. [SWS_LinSM_00156]
 * 
 */ 
void LinSM_MainFunction(void){ //[SWS_LinSM_00162], [SWS_LinSM_00159], [SWS_LinSM_00157]
   NetworkHandleType network = 1;

   if(ScheduleRequestTimer > 0){ //[SWS_LinSM_00103]
      ScheduleRequestTimer--;
      if(ScheduleRequestTimer==0){ //[SWS_LinSM_00101]
         BswM_LinSM_CurrentSchedule(network,LinSM_ScheduleTable); //[SWS_LinSM_00214] 
      }
   }
   if(GoToSleepTimer > 0){
      GoToSleepTimer--;
      if(GoToSleepTimer==0){ //[SWS_LinSM_00101]
      }
   }
   if(WakeUpTimer > 0){
      WakeUpTimer--;
      if(WakeUpTimer==0){ //[SWS_LinSM_00101]
         ComM_BusSM_ModeIndication(network,COMM_NO_COMMUNICATION); //[SWS_LinSM_00170]
         BswM_LinSM_CurrentState(network,LINSM_NO_COM);  //[SWS_LinSM_00215]
      }
   }
}

//[SWS_LinSM_00113]
/** @brief LinSM_ScheduleRequest
 * 
 * @details The upper layer requests a schedule table to be changed on one LIN network. [SWS_LinSM_00113]
 * 
 * @param [in]  network     Identification of the LIN channel
 * @param [in]  schedule    Pointer to the new Schedule table
 * 
 * @return  E_OK        Schedule table request has been accepted. \n
 *          E_NOT_OK    Not possible to perform the request, e.g. not initialized.
 */ 
Std_ReturnType LinSM_ScheduleRequest(NetworkHandleType network,LinIf_SchHandleType schedule){
//[SWS_LinSM_00241] - master configuration, so function is available
   Std_ReturnType RetVal;
   if(LinSMState == LINSM_UNINIT){
      return E_NOT_OK; //[SWS_LinSM_00116]
   }

   if(network < 1){
      return E_NOT_OK; //[SWS_LinSM_00114]
   }

   if(schedule == NULL_SCHEDULE){  
      return E_NOT_OK; //[SWS_LinSM_00115]
   }

   if(LinSMNetworkStatus != LINSM_FULL_COM){
      return E_NOT_OK; //[SWS_LinSM_10211]
   }

   ScheduleRequestTimer = LINSM_SCHEDULE_REQUEST_TIMEOUT; //[SWS_LinSM_00100]
   RetVal = LinIf_ScheduleRequest(network, schedule); //[SWS_LinSM_00079]
   if(RetVal == E_NOT_OK){
      BswM_LinSM_CurrentSchedule(network, LinSM_ScheduleTable); //[SWS_LinSM_00213]
   }
   return RetVal; // [SWS_LinSM_00168]
}

//[SWS_LinSM_00117]
/** @brief LinSM_GetVersionInfo
 * 
 * @details -- [SWS_LinSM_00117]
 * 
 * @param [out]  versioninfo    Pointer to where to store the version information of this module.
 */ 
void LinSM_GetVersionInfo ( Std_VersionInfoType* versioninfo){
   if (versioninfo == NULL){
      // [SWS_LinSM_00119]
   }
}

//[SWS_LinSM_00122]
/** @brief LinSM_GetCurrentComMode
 * 
 * @details Function to query the current communication mode. [SWS_LinSM_00122]
 * 
 * @param [in]  network Identification of the LIN channel.
 *
 * @param [out] mode    Returns the active mode, see ComM_ModeType for descriptions of the modes.
 * 
 * @return      E_OK        Ok \n
 *              E_NOT_OK    Not possible to perform the request, e.g. not initialized.
 */
Std_ReturnType LinSM_GetCurrentComMode(NetworkHandleType network,ComM_ModeType* mode){
   
   if(LinSMState == LINSM_UNINIT){
      *mode= COMM_NO_COMMUNICATION; //[SWS_LinSM_00182]
      return E_NOT_OK; //[SWS_LinSM_00125]
   }
   if(network < 1){
      return E_NOT_OK; //[SWS_LinSM_00123] 
   }
   if(mode == 0){
      return E_NOT_OK; //[SWS_LinSM_00124]
   }

   switch (LinSMNetworkStatus){
      case LINSM_RUN_COMMUNICATION:
         *mode= COMM_FULL_COMMUNICATION; //[SWS_LinSM_00181]
         break;
      default:
         *mode= COMM_NO_COMMUNICATION; //[SWS_LinSM_00180]
         break;
   }
   return E_OK;
}

//[SWS_LinSM_00126]
/** @brief LinSM_RequestComMode
 * 
 * @details Requesting of a communication mode. The mode switch will not be made instant. The LinSM will notify the caller when mode transition is made. [SWS_LinSM_00126]
 * 
 * @param [in]  network identification of the LIN channel
 *
 * @param [in]  mode    request mode
 * 
 * @return      E_OK        Request accepted \n
 *              E_NOT_OK    Not possible to perform the request, e.g. not initialized.
 */
Std_ReturnType LinSM_RequestComMode(NetworkHandleType network,ComM_ModeType mode){
   Std_ReturnType RetVal;

   if(network < 1){ //[SWS_LinSM_00127]
      return E_NOT_OK;
   }

   if(LinSMState == LINSM_UNINIT){ //[SWS_LinSM_00128]
      return E_NOT_OK;
   }

   if(mode > 3){ //[SWS_LinSM_00191]
      return E_NOT_OK;
   }

   if (mode == COMM_SILENT_COMMUNICATION){ //[SWS_LinSM_00183]
       return E_NOT_OK;
   }

   switch(mode){
      case COMM_NO_COMMUNICATION:
         if ( LinSMNetworkStatus == LINSM_FULL_COM && LinSMCommunicationStatus == LINSM_RUN_COMMUNICATION){ //[SWS_LinSM_00035]
            GoToSleepTimer = LINSM_GOTO_SLEEP_TIMEOUT; //[SWS_LinSM_00100]
            if (E_OK == LinIf_GotoSleep(network)){ //[SWS_LinSM_10208], [SWS_LinSM_00036]
               LinSMCommunicationStatus = LINSM_GOTO_SLEEP; //[SWS_LinSM_00302]
               RetVal = E_OK;
            }
            else{ //[SWS_LinSM_00177]
               GoToSleepTimer = 0;
               BswM_LinSM_CurrentState(network, LINSM_FULL_COM);
               ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION); 
               RetVal =  E_NOT_OK;
            }
         }
         else{ /*[SWS_LinSM_10209]*/ }
         break;

      case COMM_FULL_COMMUNICATION: 
         WakeUpTimer = LINSM_WAKEUP_TIMEOUT; //[SWS_LinSM_00100]
         if (E_OK == LinIf_Wakeup(network)){ //[SWS_LinSM_00047], [SWS_LinSM_00178]
            RetVal = E_OK;
         }
         else{
            WakeUpTimer = 0;
            RetVal =  E_NOT_OK; //[SWS_LinSM_00176]
         }
         break;
      default:
         break;
   }
   return RetVal;
}

//[SWS_LinSM_00129]
/** @brief LinSM_ScheduleRequestConfirmation
 * 
 * @details The LinIf module will call this callback when the new requested schedule table is active. [SWS_LinSM_00129]
 * 
 * @param [in]  network     identification of the LIN channel
 * @param [in]  schedule    pointer to the new active Schedule table
 */
void LinSM_ScheduleRequestConfirmation(NetworkHandleType network, LinIf_SchHandleType schedule){
//[SWS_LinSM_00242] - master configuration, so function is available

   if(ScheduleRequestTimer != 0){ //[SWS_LinSM_00172]
      ScheduleRequestTimer = 0; //[SWS_LinSM_00154]
      BswM_LinSM_CurrentSchedule(network,schedule); //[SWS_LinSM_00206], [SWS_LinSM_00207]
   }
}

//[SWS_LinSM_91000]
/* @brief LinSM_GotoSleepIndication
 * 
 * @details The LinIf will call this callback when the go to sleep command is received on the network or a bus idle timeout occurs. Only applicable for LIN slave nodes. [SWS_LinSM_91000]. [SWS_LinSM_00243] Function is not available due to master configuration
 * 
 * @param [in]  network     identification of the LIN channel
 */
//void LinSM_GotoSleepIndication(NetworkHandleType network){
// [SWS_LinSM_00243] Function is not available due to master configuration
//}

//[SWS_LinSM_00135]
/** @brief LinSM_GotoSleepConfirmation
 * 
 * @details The LinIf will call this callback when the go to sleep command is sent successfully or not sent successfully on the network. [SWS_LinSM_00135]
 * 
 * @param [in]  network identification of the LIN channel
 * @param [in]  success true if goto sleep was successfully sent, false otherwise
 */
void LinSM_GotoSleepConfirmation(NetworkHandleType network,boolean success){

   if(GoToSleepTimer != 0){ //[SWS_LinSM_00172]
      GoToSleepTimer = 0; //[SWS_LinSM_00154]
      if (LinSMNetworkStatus == LINSM_FULL_COM && LinSMCommunicationStatus == LINSM_GOTO_SLEEP){ //[SWS_LinSM_00046]
         LinSMNetworkStatus = LINSM_NO_COM;  //[SWS_LinSM_00193]
         ComM_BusSM_ModeIndication(network,COMM_NO_COMMUNICATION); //[SWS_LinSM_00027]
      }
   }
}

//[SWS_LinSM_00132]
/** @brief LinSM_WakeupConfirmation
 * 
 * @details The LinIf will call this callback when the wake up signal command is sent not successfully/successfully on the network. [SWS_LinSM_00132]
 * 
 * @param [in]  network identification of the LIN channel
 * @param [in]  success true if wakeup was successfully sent, false otherwise
 */
void LinSM_WakeupConfirmation(NetworkHandleType network,boolean success){

   if(WakeUpTimer!=0){  //[SWS_LinSM_00172]
      WakeUpTimer=0; //[SWS_LinSM_00154]  
      if(TRUE == success){
         LinSMNetworkStatus = LINSM_FULL_COM; //[SWS_LinSM_00049]
         BswM_LinSM_CurrentState(network, LINSM_FULL_COM);  // [SWS_LinSM_00192]
         
         LinSMCommunicationStatus = LINSM_RUN_COMMUNICATION; //[SWS_LinSM_00301]
         ComM_BusSM_ModeIndication(network,COMM_FULL_COMMUNICATION); //[SWS_LinSM_00033]
      }
      else{
         BswM_LinSM_CurrentState(network, LINSM_NO_COM); //[SWS_LinSM_00202]
         ComM_BusSM_ModeIndication(network,COMM_NO_COMMUNICATION); 
      }
   }
}

/* Satisfied requirements:
* [SWS_LinSM_00019] - satisfied
* [SWS_LinSM_00020] - satisfied
* [SWS_LinSM_00028] - satisfied
* [SWS_LinSM_00173] - satisfied
* [SWS_LinSM_00022] - satisfied
* [SWS_LinSM_00024] - satisfied
* [SWS_LinSM_00026] - satisfied
* [SWS_LinSM_00032] - satisfied
*/

/*------------------------------------------------------------
  ----------------------End-of-file---------------------------
  ------------------------------------------------------------*/
